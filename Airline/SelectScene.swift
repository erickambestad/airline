//
//  SelectScene.swift
//  Airline
//
//  Created by Eric Kambestad on 6/26/15.
//  Copyright (c) 2015 Eric Kambestad. All rights reserved.
//

import Foundation
import SpriteKit

/*
 * Select aircraft scene with some random aircraft images I found on Google.  Cooper's favorites.
 */

class SelectScene: SKScene {
    
    var southwest = SKSpriteNode(imageNamed: "southwest")
    var bluehelicopter = SKSpriteNode(imageNamed: "bluehelicopter")
    var dusty = SKSpriteNode(imageNamed: "dusty")
    var xwing = SKSpriteNode(imageNamed: "xwing")

    // Place all aircrafts in a 4 box grid
    override func didMoveToView(view: SKView) {
        
        // screen color.. blue
        self.backgroundColor = UIColor.whiteColor()
        
        let selectText = SKLabelNode(text: "SELECT YOUR AIRCRAFT")
        selectText.position.y = self.size.height/4
        selectText.fontColor = UIColor.blackColor()
        selectText.fontSize = 72.0
        self.addChild(selectText)
        
        southwest.name = "southwest"
        southwest.position.x = (self.size.width / 5) * -1
        self.addChild(southwest)
        
        bluehelicopter.name = "bluehelicopter"
        bluehelicopter.position.x = self.size.width / 5
        self.addChild(bluehelicopter)
        
        dusty.name = "dusty"
        dusty.position.x = (self.size.width / 5) * -1
        dusty.position.y = (self.size.height / 4) * -1
        self.addChild(dusty)
        
        xwing.name = "xwing"
        xwing.position.x = self.size.width / 5
        xwing.position.y = (self.size.height / 4) * -1
        self.addChild(xwing)
    }
    
    
    /*
     *  Aircraft selected, move to main scene and start flying
     */
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch in (touches as! Set<UITouch>) {
            let location = touch.locationInNode(self)
            var planes = nodesAtPoint(location)
            
            for plane in planes {
                if let planeNode = plane as? SKSpriteNode {
                    if let name = planeNode.name {
                        
                        let gameScene = GameScene(size: self.size)
                        gameScene.scaleMode = .ResizeFill
                        gameScene.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                        gameScene.playerSelected = name
                        let reveal = SKTransition.fadeWithDuration(1)
                        self.view?.presentScene(gameScene, transition: reveal)

                    }
                }
            }
        }
        
    }
}