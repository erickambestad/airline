//
//  Plane.swift
//  Airline
//
//  Created by Eric Kambestad on 6/24/15.
//  Copyright (c) 2015 Eric Kambestad. All rights reserved.
//

import Foundation
import SpriteKit

class Plane {
    var plane: SKSpriteNode
    var speed = 0.1
    var emit = false
    var emitFrameCount = 0
    var maxEmitFrameCount = 3
    var particles: SKEmitterNode
    var zPos: CGFloat = 4.0
    
    init (plane: SKSpriteNode, particles: SKEmitterNode) {
        self.plane = plane
        self.particles = particles
    }
}