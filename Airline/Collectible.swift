//
//  Coin.swift
//  Airline
//
//  Created by Eric Kambestad on 6/24/15.
//  Copyright (c) 2015 Eric Kambestad. All rights reserved.
//

import Foundation
import SpriteKit

class Collectible {
    var speed: Float = 0.0
    var item: SKSpriteNode
    var currentFrame = 0
    var randomFrame = 0
    var moving = false
    var angle = 0.0
    var range = 2.0
    var yPos = CGFloat()
    
    init(speed: Float, item: SKSpriteNode) {
        self.speed = speed
        self.item = item
        self.setRandomFrame()
    }
    
    func setRandomFrame() {
        var range = UInt32(100)..<UInt32(400)
        self.randomFrame = Int(range.startIndex + arc4random_uniform(range.endIndex - range.startIndex + 1))
    }
}