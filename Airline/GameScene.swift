//
//  GameScene.swift
//  Airline
//
//  Created by Eric Kambestad on 6/24/15.
//  Copyright (c) 2015 Eric Kambestad. All rights reserved.
//

import SpriteKit
import AVFoundation

/**
 * Main game scene. 
 *
 * Plane flies around the screen and collects collectibles. 
 * This is a game for my 2 year old.. the only way to win is to enjoy the cool aircraft and characters flying around the screen.. which he does.
 */

class GameScene: SKScene, SKPhysicsContactDelegate {
    var playerSelected = ""
    var player:Plane!
    var touchLocation = CGFloat()
    var gameOver = false
    var collectibles: [Collectible] = []
    var collectibleCount = 2
    var clouds: [Cloud] = []
    var endOfScreenRight = CGFloat()
    var endOfScreenLeft = CGFloat()
    var endOfScreenTop = CGFloat()
    var endOfScreenBottom = CGFloat()
    
    var cloudSpeed: CGFloat = 1.0
    
    // score
    var score = 0
    var scoreLabel = SKLabelNode()
    
    // audio
    var aircraftSound = NSURL()
    var audioPlayer = AVAudioPlayer()
    var coinSound = NSURL()
    var coinPlayer = AVAudioPlayer()
    
    enum ColliderType: UInt32 {
        case Plane = 1
        case Item = 2
    }
    
    
    /* 
     * Set up the screen
     * - set up the properties / background
     * - add all of the initial elemets (clouds, plane, starting collectibles, sounds, etc.)
     */
    override func didMoveToView(view: SKView) {
        self.physicsWorld.contactDelegate = self
        
        // helpful screen properties
        endOfScreenLeft = (self.size.width / 2) * CGFloat(-1)
        endOfScreenRight = self.size.width / 2
        endOfScreenBottom = (self.size.height / 2) * CGFloat(-1)
        endOfScreenTop = self.size.height / 2
        
        // screen color.. blue.
        self.backgroundColor = UIColor(red: 203/255, green: 250/255, blue: 255/255, alpha: 1)
        
        // Add the clouds
        self.addClouds()
        
        // Add the selected plane
        self.addPlane()
        
        // Add the coins
        self.addCollectibles()
        
        // Add the character collectibles
        self.addSpecialCollectible()
        
        // Set the collectible timers to 2 and 6 seconds for the different types
        var collectibleTimer = NSTimer.scheduledTimerWithTimeInterval(6.0, target: self, selector: "addCollectibles", userInfo: nil, repeats: true)
        var specialCollectibleTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "addSpecialCollectible", userInfo: nil, repeats: true)
        
        // Set up the score label and add to scene
        scoreLabel = SKLabelNode(text: "0")
        scoreLabel.position.y = -(self.size.height/4)
        scoreLabel.fontColor = UIColor.blackColor()
        addChild(scoreLabel)
        
        
        // Set up the back button and add to screen
        let backButton = SKSpriteNode(imageNamed: "back")
        backButton.name = "back"
        backButton.position.x = ((self.size.width / 2) * -1) + 50
        backButton.position.y = self.size.height / 2 - 50
        backButton.size.width = 50
        backButton.size.height = 50
        addChild(backButton)
        
        
        // start the aircraft sounds
        aircraftSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource(playerSelected, ofType: "wav")!)!
        audioPlayer = AVAudioPlayer(contentsOfURL: aircraftSound, error: nil)
        audioPlayer.prepareToPlay()
        audioPlayer.numberOfLoops = -1
        audioPlayer.play()
        
        
        // prepare sounds for coins
        coinSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("coin", ofType: "wav")!)!
        coinPlayer = AVAudioPlayer(contentsOfURL: coinSound, error: nil)
        coinPlayer.prepareToPlay()
    }
    
    
    /*
     *  Check the contacts and collect/score if the user hits a collectible
     */
    func didBeginContact(contact: SKPhysicsContact) {
        
        // if the plane hits a collectible, remove it from the screen
        if contact.bodyA.categoryBitMask == 1 && contact.bodyB.categoryBitMask == 2 {
            contact.bodyB.node?.removeFromParent()
        }
        
        // Start the spark emitter
        player.emit = true
        
        var straighten = SKAction.rotateToAngle(0.0, duration: NSTimeInterval(5))
        player.plane.runAction(straighten)
        
        // Update the score label
        updateScore()
        
        // Play the collect coin sound
        coinPlayer.play()
    }
    
    /*
     * Add the clouds.. currently just 1, don't need the clutter
     */
    func addClouds() {
        addCloud(named: "cloud", speed: 1.0)
    }
    
    /*
     * Adds each cloud, 1 at a time
     */
    func addCloud(#named:String, speed:CGFloat) {
        var cloudNode = SKSpriteNode(imageNamed: named)
        
        // random height
        let y_pos = CGFloat(Float(arc4random()) / Float(UINT32_MAX)) * endOfScreenTop
        
        cloudNode.zPosition = CGFloat(1.0)
        cloudNode.position.x = endOfScreenRight + 300
        cloudNode.position.y = y_pos
        cloudNode.alpha = 0.4
        
        var cloud = Cloud(cloud: cloudNode)
        cloud.speed = CGFloat(speed)
        cloud.zPos = 1.0
        clouds.append(cloud)
        addChild(cloudNode)
    }
    
    
    /*
     * Add the user plane and set its physics and particle emitter
     */
    func addPlane() {
        let playerNode = SKSpriteNode(imageNamed: playerSelected)
        playerNode.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(playerNode.size.width, playerNode.size.height))
        playerNode.physicsBody!.affectedByGravity = false
        playerNode.physicsBody!.categoryBitMask = ColliderType.Plane.rawValue
        playerNode.physicsBody!.contactTestBitMask = ColliderType.Item.rawValue
        playerNode.physicsBody!.collisionBitMask = ColliderType.Item.rawValue
        let planeParticles = SKEmitterNode(fileNamed: "HitParticle.sks")
        planeParticles.particleSize = CGSizeMake(40,40)
        planeParticles.hidden = true
        player = Plane(plane: playerNode, particles: planeParticles)
        playerNode.position = CGPointMake(-(self.size.width/4), 0.0)
        playerNode.addChild(planeParticles)
        playerNode.zPosition = player.zPos
        addChild(playerNode)
    }
    
    
    /*
     * Adds collectibles (coins), half to the top of the screen and half below
     */
    func addCollectibles() {
        for newCollectible in 1...self.collectibleCount/2 {
            addCollectible(named: "coin", speed: Float(self.randomNumber(range: 10...50)/10), yPos: CGFloat(self.randomNumber(range: 0...Int(self.size.height/3))))
            addCollectible(named: "coin", speed: Float(self.randomNumber(range: 10...50)/10), yPos: (CGFloat(self.randomNumber(range: 0...Int(self.size.height/3))) * -1))
        }
    }
    
    
    /*
     * Adds each collectible (coin) and sets physics, 1 at a time.
     */
    func addCollectible(#named:String, speed:Float, yPos:CGFloat) {
        var collectibleNode = SKSpriteNode(imageNamed: named)
        collectibleNode.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(collectibleNode.size.width, collectibleNode.size.height))
        collectibleNode.physicsBody!.affectedByGravity = false
        collectibleNode.physicsBody!.categoryBitMask = ColliderType.Item.rawValue
        collectibleNode.physicsBody!.contactTestBitMask = ColliderType.Plane.rawValue
        collectibleNode.physicsBody!.collisionBitMask = ColliderType.Plane.rawValue
        
        var collectible = Collectible(speed: speed, item: collectibleNode)
        self.collectibles.append(collectible)
        self.resetCollectible(collectibleNode, yPos: yPos)
        collectible.yPos = collectibleNode.position.y
        addChild(collectibleNode)
    }
    
    
    /*
     * Adds the special collectible.  "specials" array hold the image names.  Adds 2 random collectibles from that array when fired.
     */
    func addSpecialCollectible() {
        let specials = ["minion", "mack", "mickey", "football", "nemo", "buzz", "olaf", "snotrod", "sven", "mcqueen", "tractor", "miles", "thomas"]
        self.addCollectible(named: specials[self.randomNumber(range: 0...specials.count-1)], speed: Float(self.randomNumber(range: 10...50)/10), yPos: CGFloat(self.randomNumber(range: 0...Int(self.size.height/3))))
        self.addCollectible(named: specials[self.randomNumber(range: 0...specials.count-1)], speed: Float(self.randomNumber(range: 10...50)/10), yPos: (CGFloat(self.randomNumber(range: 0...Int(self.size.height/3))) * -1))
    }
    
    func resetCollectible(collectibleNode: SKSpriteNode, yPos:CGFloat) {
        collectibleNode.position.x = endOfScreenRight
        collectibleNode.position.y = yPos
    }
    
    // Loops through clouds and runs the animation to make it look like we're moving
    func updateCloudPosition() {
        for c in self.clouds {
                let cloudAction = SKAction.moveToX(endOfScreenLeft - 400, duration: 5)
                c.cloud.runAction(cloudAction) {
                    c.cloud.removeFromParent()
                    self.clouds.removeLast()
                    self.addCloud(named: "cloud", speed: 1.0)
                }
        }
    }
    
    /*
     *
     */
    func updateCoinPosition() {
        for collectible in self.collectibles {
            if !collectible.moving {
                collectible.currentFrame++
                if collectible.currentFrame > collectible.randomFrame {
                    collectible.moving = true
                }
            } else {
                collectible.item.position.y = CGFloat(Double(collectible.item.position.y) + sin(collectible.angle) * collectible.range)
                collectible.angle += player.speed
                if collectible.item.position.x > endOfScreenLeft {
                    collectible.item.position.x -= CGFloat(collectible.speed)
                } else {
                    // you lost that one.. remove from screen
                    collectible.item.removeFromParent()
                }
            }
        }
    }
    
    
    // Toggles the spark emitter
    func updatePlayerEmitter() {
        if player.emit && player.emitFrameCount < player.maxEmitFrameCount {
            player.emitFrameCount++
            player.particles.hidden = false
        } else {
            player.emit = false
            player.particles.hidden = true
            player.emitFrameCount = 0
        }
    }
    
    
    // Updates the score label
    func updateScore() {
        score++
        scoreLabel.text = String(score)
    }
    
    
    /*
     * Responsible for moving the plane around the screen
     */
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch in (touches as! Set<UITouch>) {
            if !self.gameOver {
                
                var touch_x = touch.locationInView(self.view!).x - (self.size.width / 2)
                
                // don't let the plane go all the way right
                if touch_x > 0.0 {
                    touch_x = 0.0
                }
                
                var touch_y = (touch.locationInView(self.view!).y * -1) + (self.size.height / 2)
                
                if (touch.locationInNode(self).y > player.plane.position.y) {
                    var flyUp = SKAction.rotateToAngle(CGFloat(3.14/5), duration: NSTimeInterval(0.5))
                    player.plane.runAction(flyUp)
                } else {
                    var flyDown = SKAction.rotateToAngle(CGFloat(-3.14/5), duration: NSTimeInterval(0.5))
                    player.plane.runAction(flyDown)
                }
                
                let moveAction = SKAction.moveTo(CGPoint(x: CGFloat(touch_x), y: CGFloat(touch_y)), duration: 1.2)
                moveAction.timingMode = SKActionTimingMode.EaseOut
                player.plane.runAction(moveAction) {
                    // moved action
                }
            }
            
            let location = touch.locationInNode(self)
            var nodes = nodesAtPoint(location)
            
            for touchNode in nodes {
                if let tn = touchNode as? SKSpriteNode {
                    if let name = tn.name {
                        if name == "back" {
                            audioPlayer.stop()
                            let selectScene = SelectScene(size: self.size)
                            selectScene.scaleMode = .ResizeFill
                            selectScene.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                            let flip = SKTransition.flipHorizontalWithDuration(1)
                            self.view?.presentScene(selectScene, transition: flip)
                        }
                    }
                }
            }
            
            
        }
    }
    
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        /* Called when a touch ends */
        
        for touch in (touches as! Set<UITouch>) {
            var straighten = SKAction.rotateToAngle(0.0, duration: NSTimeInterval(0.5))
            player.plane.runAction(straighten)
        }
    }
   
    // if game isn't over (not a thing atm), send the coins and clouds across the screen
    override func update(currentTime: CFTimeInterval) {
        if !gameOver {
            self.updateCloudPosition()
            self.updateCoinPosition()
        }
        updatePlayerEmitter()
    }
    
    
    /*
     * Gets the random number within a passed in range
     */
    func randomNumber(range: Range<Int> = 1...6) -> Int {
        let min = range.startIndex
        let max = range.endIndex
        return Int(arc4random_uniform(UInt32(max - min))) + min
    }
}
