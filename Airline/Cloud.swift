//
//  Cloud.swift
//  Airline
//
//  Created by Eric Kambestad on 6/24/15.
//  Copyright (c) 2015 Eric Kambestad. All rights reserved.
//

import Foundation
import SpriteKit

class Cloud {
    var cloud: SKSpriteNode
    var speed: CGFloat = 1.0
    var zPos: CGFloat = 0.0
    
    init(cloud: SKSpriteNode) {
        self.cloud = cloud
    }
}